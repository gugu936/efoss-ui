import { NgModule } from "@angular/core";
import { SearchBarComponentComponent } from "./search-bar-component/search-bar-component.component";
import { CustomComponentComponent } from './custom-component.component';

@NgModule({
    imports: [
    ],
    declarations: [
        CustomComponentComponent,
        SearchBarComponentComponent
    ],
    exports: [
        CustomComponentComponent,
        SearchBarComponentComponent
    ]
  })
export class CustomComponentsModule { }