export const orderStatuses = [
    {
        id   : 1,
        name : 'Payment accepted',
        color: 'green-500'
    },
    {
        id   : 2,
        name : 'Preparing the order',
        color: 'orange-500'
    },
    {
        id   : 3,
        name : 'Shipped',
        color: 'purple-500'
    },
    {
        id   : 4,
        name : 'Delivered',
        color: 'green-800'
    },
    {
        id   : 7,
        name : 'Refunded',
        color: 'red-500'
    },
    {
        id   : 9,
        name : 'On pre-order (paid)',
        color: 'purple-300'
    },
    {
        id   : 10,
        name : 'On pre-order (not paid)',
        color: 'purple-300'
    },
];
