import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { orderStatuses } from 'app/main/apps/e-commerce/order/order-statuses';
import { EcommerceOrderService } from 'app/main/apps/e-commerce/order/order.service';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from 'app/entities/order/order.service';
import { IOrder, Order } from 'app/shared/model/order.model';
import { IOrderDTO, OrderDTO } from 'app/shared/model/order.dto';
import { SERVER_API_URL } from 'app/app.constants';
import * as _ from 'lodash';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { IShipping, Shipping } from 'app/shared/model/shipping.model';
import { ShippingService } from 'app/entities/shipping/shipping.service';

@Component({
    selector: 'e-commerce-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class EcommerceOrderComponent implements OnInit, OnDestroy {
    orderStatuses: any;
    statusForm: FormGroup;
    shipForm: FormGroup;
    order?: IOrderDTO;
    update?: IOrder;
    resourceUrl = SERVER_API_URL + 'api/product-images/get-by-product-id/'
    // Private
    private _unsubscribeAll: Subject<any>;

    get orderStatus() {
        const a = _.find(this.orderStatuses || [], status => Number(status['id']) === Number(this.order.status))
        return a
    }
    /**
     * Constructor
     *
     * @param {EcommerceOrderService} _ecommerceOrderService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _ecommerceOrderService: EcommerceOrderService,
        private _route: ActivatedRoute,
        private _orderService: OrderService,
        private _shippingService: ShippingService,
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.order = new OrderDTO();
        this.update = new Order()
        this.orderStatuses = orderStatuses;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.statusForm = this.createStatusForm()
        this._route.params
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => {
                this.shipForm = this.createShipForm()
                this._orderService.find(params.id).subscribe(res => {
                    this.order = res.body
                    console.log(this.order.billingAddress)
                    this.path()
                }, error => console.log(error))
            })
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    path(): void {
        this.shipForm.patchValue({
            trackingCode:  _.get(this.order.shipping, 'trackingCode' ,''),
            shippingCarrier:  _.get(this.order.shipping, 'carrier' ,''),
            shippingWeight: _.get(this.order.shipping, 'weight' ,''),
            createdAt: this.order.shipping ? this.order.shipping.createdAt.toString() : '',
            shippingFee:  _.get(this.order.shipping, 'fee' ,''),
        })
    }

    updateShipping(): void {
        this._shippingService.update(this.createFromForm()).subscribe(res => {
             this.order.shipping = res.body;
        }, error => console.log(error))
    }

    private createFromForm(): IShipping {
        return {
          ...new Shipping(),
          id: _.get(this.order.shipping, 'id', '') || '',
          orderId: this.order.id,
          trackingCode: this.shipForm.get(['trackingCode'])!.value,
          carrier: this.shipForm.get(['shippingCarrier'])!.value,
          weight: this.shipForm.get(['shippingWeight'])!.value,
          fee: this.shipForm.get(['shippingFee'])!.value,
        };
      }

    createShipForm(): FormGroup {
        return this._formBuilder.group({
            id: [''],
            trackingCode: [''],
            shippingCarrier: [''],
            shippingWeight: [''],
            createdAt: [''],
            shippingFee: [''],
 
            // trackingCode: [''],
            // shippingCarrier: [''],
            // shippingWeight: [''],
            // createdAt: [''],
            // shippingFee: [''],
        });
    }

    createStatusForm(): FormGroup {
        return this._formBuilder.group({
            newStatus: [this.order.status],
        });
    }

    updateStatus(): void {
        this.order.status = this.statusForm.controls.newStatus.value
        this.update.id = this.order.id;
        this.update.cartId = this.order.cartId;
        this.update.companyId = this.order.company.id;
        this.update.paymentMethodId = this.order.paymentMethod.id;
        this.update.total = this.order.total
        this.update.cartItemIds = _.map(this.order.cartItems||[], item => item.id)
        this.update.status = this.order.status 
        this._orderService.update(this.update).subscribe(res => {
            console.log('res', res.body)
        }, error => console.log(error))
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
}
