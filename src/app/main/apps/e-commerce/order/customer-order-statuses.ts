export const customerOrderStatuses = [
    {
        id   : 11,
        name : 'Awaiting check payment',
        color: 'blue-500'
    },
        {
        id   : 5,
        name : 'Canceled',
        color: 'pink-500'
    },
        {
        id   : 8,
        name : 'Payment error',
        color: 'red-900'
    },
        {
        id   : 13,
        name : 'Awaiting bank wire payment',
        color: 'blue-500'
    },
    {
        id   : 12,
        name : 'Remote payment accepted',
        color: 'green-500'
    },
    {
        id   : 14,
        name : 'Awaiting Cash-on-delivery payment',
        color: 'blue-500'
    }
]