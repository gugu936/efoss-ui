import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { takeUntil } from 'rxjs/internal/operators';
import { ProductService } from 'app/entities/product/product.service';
import { IProduct } from 'app/shared/model/product.model';
import { HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import {storage} from 'app/storage.util'
import { IProductDTO } from 'app/shared/model/product.dto';
import { AccountService } from 'app/core/auth/account.service';
import { Router } from '@angular/router';

@Component({
    selector: 'e-commerce-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class EcommerceProductsComponent implements OnInit {
    displayedColumns = ['image', 'name', 'category_id', 'price', 'qty', 'active'];
    products?: IProductDTO[] = [];
    itemsPerPage = 10
    predicate = 'id'
    ascending = 'desc'
    page = 0
    resourceUrl = SERVER_API_URL + 'api/product-images'
    search = ''

    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;

    @ViewChild(MatSort, { static: true })
    sort: MatSort;

    @ViewChild('filter', { static: true })
    filter: ElementRef;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _accountService: AccountService,
        private  _router: Router,
        private _productsService: ProductService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._accountService.identity().subscribe(account => {
            if (!account) {
               this._router.navigate(['/pages/auth/login']);
            } else {
                this.loadAll()
            }
        })
    }

    sortData(e): void {
        console.log(e)
        this.predicate = e.active
        this.ascending = e.direction
        this.loadAll()
    }


    goSearch() {
        this.page = 0
        this._productsService.search(
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.querySort()
            }, this.search
        ).subscribe((res: HttpResponse<IProductDTO[]>) => (this.products = res.body || []));
    }

    errorHandler(event):void {
        event.target.src='assets/images/ecommerce/product-image-placeholder.png'
    }

    pageEvent(e): void {
        console.log(e)
        this.page = e.pageIndex
        this.itemsPerPage = e.pageSize
        this.loadAll()
    }

    getRecord(product) {
        console.log('product', product)
    }

    loadAll(): void {
        console.log('  this.loadAll()')
        this._productsService.query(
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.querySort()
            }
        ).subscribe((res: HttpResponse<IProductDTO[]>) => (this.products = res.body || []));
    }

    trackId(index: number, item: IProductDTO): string {
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
        return item.id!;
    }

    querySort(): string[] {
        const result = [this.predicate + ',' + (this.ascending || 'asc')];
        // if (this.predicate !== 'id') {
        //   result.push('id');
        // }
        return result;
    }
}


