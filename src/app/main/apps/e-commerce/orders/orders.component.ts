import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { EcommerceOrdersService } from 'app/main/apps/e-commerce/orders/orders.service';
import { takeUntil } from 'rxjs/internal/operators';
import { IOrderDTO } from 'app/shared/model/order.dto';
import { OrderService } from 'app/entities/order/order.service';
import { HttpResponse } from '@angular/common/http';
import * as _ from 'lodash';
import { orderStatuses } from '../order/order-statuses';

@Component({
    selector: 'e-commerce-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class EcommerceOrdersComponent implements OnInit, OnDestroy {
    displayedColumns = ['id', 'customer', 'total', 'payment', 'status', 'date'];
    orderStatuses: any;
    orders?: IOrderDTO[] = [];
    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;
    itemsPerPage = 10
    search = '';
    predicate = 'id'
    ascending = 'desc'
    page = 0
    @ViewChild('filter', { static: true })
    filter: ElementRef;

    @ViewChild(MatSort, { static: true })
    sort: MatSort;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {EcommerceOrdersService} _ecommerceOrdersService
     */
    constructor(
        private _orderService: OrderService,
        private _ecommerceOrdersService: EcommerceOrdersService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.orderStatuses = orderStatuses;
        this.loadAll()
    }

    pageEvent(e): void {
        console.log(e)
        this.page = e.pageIndex
        this.itemsPerPage = e.pageSize
        this.loadAll()
    }

    getStatusName(id) {
        return _.find(orderStatuses || [], s => Number(s.id) === Number(id))
    }

    goSearch(): void {
        this.page = 0
        this._orderService.search({
            page: this.page,
            size: this.itemsPerPage,
            sort: this.querySort()
        }, this.search).subscribe(res => {
            this.orders = res.body
        })
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    loadAll(): void {
        console.log('  this.loadAll()')
        this._orderService.queryById(
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.querySort()
            }
        ).subscribe((res: HttpResponse<IOrderDTO[]>) => (this.orders = res.body || []));
    }

    sortData(e): void {
        console.log(e)
        this.predicate = e.active
        this.ascending = e.direction
        this.loadAll()
    }

    querySort(): string[] {
        const result = [this.predicate + ',' + (this.ascending || 'asc')];
        return result;
    }
}

