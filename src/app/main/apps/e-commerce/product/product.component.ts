import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import * as moment from 'moment';
import { ProductService } from 'app/entities/product/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { IProduct, Product } from 'app/shared/model/product.model';
import { ProductImageService } from 'app/entities/product-image/product-image.service';
import { SERVER_API_URL } from 'app/app.constants';
import * as _ from 'lodash'
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AccountService } from 'app/core/auth/account.service';
import { Category } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { ProductDTO, IProductDTO } from 'app/shared/model/product.dto';
import { map, startWith } from 'rxjs/operators';

@Component({
    selector: 'e-commerce-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class EcommerceProductComponent implements OnInit, OnDestroy {
    myControl = new FormControl();
    product: IProduct;
    pageType: string;
    productForm: FormGroup;
    productId = '0';
    resourceUrl = SERVER_API_URL + 'api/product-images/get-by-product-id/'
    files: File[] = [];
    account: any;
    images = []
    options: Category[]
    filteredOptions: Observable<Category[]>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ProductService} _productService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     */
    constructor(
        private _productService: ProductService,
        private _formBuilder: FormBuilder,
        private _accountService: AccountService,
        private _location: Location,
        private _categoryService: CategoryService,
        private _router: Router,
        private _productImageService: ProductImageService,
        private _route: ActivatedRoute,
        private _matSnackBar: MatSnackBar
    ) {
        // Set the default
        this.product = new Product();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._route.params
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(params => {
                const id = params['id'];
                this.productForm = this.createProductForm();
                if (id && id !== 'new') {
                    this._productImageService.getImageCount(id).subscribe(res => {
                        for (let i = 0; i < res.body; i++) {
                            this.images.push(i)
                        }
                    }, error => {
                        console.log(error)
                    })
                    this.pageType = 'edit';
                    this.productId = id
                    this._productService.find(id).subscribe((res: HttpResponse<IProduct>) => {
                        this.product = res.body
                        this.path()
                    });
                } else {
                    this._productService.create(this.product).subscribe(res => {
                        console.log(res.body)
                        this.product = res.body
                        this.productId = this.product.id
                        this.path()
                    }, error => { console.log(error) })
                    this.pageType = 'new';
                }

                console.log('id', id)
            });
        this._categoryService.query().subscribe(res => {
            this.options = res.body
            const selectedCategory = _.find(this.options || [], option => option.id === this.product.categoryId)
            console.log('selected', selectedCategory, this.product.categoryId)
            this.myControl.setValue(selectedCategory)
            this.filteredOptions = this.myControl.valueChanges
                .pipe(
                    startWith(''),
                    map(value => typeof value === 'string' ? value : value.id),
                    map(name => name ? this._filter(name) : this.options.slice())
                );
        }, error => console.log(error))

        this._accountService.identity().subscribe(account => {
            if (account) {
                this.account = account
            } else {
                this._router.navigate(['/pages/auth/login']);
            }
        })
    }

    deleteImage(seq): void {
        this._productImageService.deleteById(this.productId, seq).subscribe(res => {
            console.log(res.body)
            this.images.splice(seq, 1)
        }, error => {
            console.log(error)
        })
    }

    addSize(e): void {
        if (!Array.isArray(this.product.size)) {
            this.product.size = []
        }
        if ((e.value || '').trim()) {
            this.product.size.push(e.value.trim());
            this.productForm.markAsDirty()
        }

        // Reset the input value
        if (e.input) {
            e.input.value = '';
        }
    }

    removeSize(size): void {
        const index = this.product.size.indexOf(size);

        if (index >= 0) {
          this.product.size.splice(index, 1);
        }
    }

    addColor(e): void {
        if (!Array.isArray(this.product.color)) {
            this.product.color = []
        }
        if ((e.value || '').trim()) {
            this.product.color.push(e.value.trim());
            this.productForm.markAsDirty()
        }

        // Reset the input value
        if (e.input) {
            e.input.value = '';
        }
    }

    removeColor(color): void {
        const index = this.product.color.indexOf(color);

        if (index >= 0) {
          this.product.color.splice(index, 1);
        }
    }

    path(): void {
        this.productForm.patchValue({
            id: this.product.id,
            name: this.product.name,
            description: this.product.description,
            qty: this.product.qty,
            brand: this.product.brand,
            shippingFee: this.product.shippingFee,
            price: this.product.price,
            weight: this.product.weight,
            height: this.product.height,
            depth: this.product.depth,
            width: this.product.width,
            size: this.product.size,
            color: this.product.color,
            refurbished: this.product.refurbished,
            active: this.product.active
        })
    }

    drop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.images, event.previousIndex, event.currentIndex)
        console.log(this.images);
        this._productImageService.reorder({
            seq: this.images,
            id: this.productId
        }).subscribe(res => {
            console.log('res', res)
        }, error => { console.log(error) })
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    onSelect(event) {
        this.files.push(...event.addedFiles);
        _.forEach(event.addedFiles || [], (file, i: number) => {
            console.log('seq', i)
            const formData = new FormData()
            formData.append('file', file, file.name);
            formData.append('productId', this.product.id)
            formData.append('seq', i + '')
            this._productImageService.uploadOne(formData).subscribe(
                (res) => {
                    console.log('abcdef i', i)
                    this.files.splice(0, 1);
                    this.images.push(this.images.length)
                },
                (error) => console.log('failed', error)
            );
        })
    }

    displayFn(category: Category): string {
        return category && category.name ? category.name : '';
    }

    private _filter(name: string): Category[] {
        console.log('name', name)
        this.productForm.markAsDirty()
        const filterValue = name.toLowerCase();

        return this.options.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
    }

    onRemove(event) {
        console.log(event);
        this.files.splice(this.files.indexOf(event), 1);
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create product form
     *
     * @returns {FormGroup}
     */
    createProductForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.product.id],
            name: [this.product.name],
            description: [this.product.description],
            qty: [this.product.qty],
            brand: [this.product.brand],
            shippingFee: [this.product.shippingFee],
            weight: [this.product.weight],
            price: [this.product.price],
            depth: [this.product.depth],
            width: [this.product.width],
            height: [this.product.height],
            size: [this.product.size],
            color: [this.product.color],
            refurbished: [this.product.refurbished],
            active: [this.product.active]
        });
    }

    private createFromForm(): IProduct {
        return {
            ...new Product(),
            id: this.productForm.get(['id'])!.value,
            name: this.productForm.get(['name'])!.value,
            qty: this.productForm.get(['qty'])!.value,
            description: this.productForm.get(['description'])!.value,
            companyId: this.account.companyId,
            brand: this.productForm.get(['brand'])!.value,
            categoryId: this.myControl.value.id,
            price: this.productForm.get(['price'])!.value,
            shippingFee: this.productForm.get(['shippingFee'])!.value,
            weight: this.productForm.get(['weight'])!.value,
            active: this.productForm.get(['active'])!.value,
            height: this.productForm.get(['height'])!.value,
            depth: this.productForm.get(['depth'])!.value,
            width: this.productForm.get(['width'])!.value,
            refurbished: this.productForm.get(['refurbished'])!.value,
            size: this.product.size,
            color: this.product.color,
            // createdAt:  moment(this.product.createdAt, DATE_TIME_FORMAT),
            // updatedAt:  moment(this.product.updatedAt, DATE_TIME_FORMAT)
        };
    }

    /**
     * Save product
     */
    saveProduct(): void {
        const product = this.createFromForm();
        this._productService.update(product).subscribe(res => {
            this._matSnackBar.open('Save success', '', { duration: 2500 });
            this._router.navigate(['/apps/e-commerce/products']);
        }, error => {
            console.log('error', error)
        })
    }

    /**
     * Add product
     */
    addProduct(): void {

    }
}
