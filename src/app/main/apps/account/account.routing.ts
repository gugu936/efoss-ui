import { Routes } from "@angular/router";
import { AccountComponent } from './account.component';
import { SettingsComponent } from './settings/settings.component';

export const accountRoutes: Routes = [
    {
        path: '',
        component: AccountComponent
    },
    {
        path: 'settings',
        component: SettingsComponent
    }
]
