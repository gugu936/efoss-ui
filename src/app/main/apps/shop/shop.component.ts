import { Component, OnInit, AfterViewInit } from "@angular/core";

@Component({
    selector: 'app-shop',
    templateUrl: 'shop.component.html',
    styleUrls: ['shop.component.scss']
})
export class ShopComponent implements OnInit, AfterViewInit {

    constructor() { }

    ngOnInit(): void {

    }

    ngAfterViewInit(): void {

    }

}