import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { ShopRoutes } from "./shop.routing";
import { ShopDetailComponent } from "./detail/detail.component";
import { ShopListComponent } from "./list/list.component";
import { ShopComponent } from "./shop.component";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ShopRoutes),
    ],
    providers: [],
    entryComponents: [],
    declarations: [
        ShopComponent,
        ShopDetailComponent,
        ShopListComponent
    ]
})
export class ShopModule { }