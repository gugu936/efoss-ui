import { Routes } from "@angular/router";
import { ShopListComponent } from "./list/list.component";
import { ShopComponent } from "./shop.component";

export const ShopRoutes: Routes = [
    {
        path: '',
        component: ShopComponent
    },
    {
        path: 'list',
        component: ShopListComponent
    },
]