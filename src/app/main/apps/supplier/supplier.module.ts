import { ProductComponent } from "./product/product.component";
import { OrderComponent } from "./order/order.component";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { supplierRoutes } from "./supplier.routing";
import { SupplierComponent } from "./supplier.component";

@NgModule({
    imports: [
        RouterModule.forChild(supplierRoutes),

    ],
    providers: [],
    entryComponents: [],
    declarations: [
        SupplierComponent,
        ProductComponent,
        OrderComponent
    ]
})
export class SupplierModule { }