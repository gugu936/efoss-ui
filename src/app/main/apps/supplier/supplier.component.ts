import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AccountService } from 'app/core/auth/account.service';

declare var require: any;

const data: any = require('./data.json');

export interface Chart {
	options?: any;
	responsiveOptions?: any;
}

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.scss']
})
export class SupplierComponent implements OnInit {
  account: Account | null = null;
	authSubscription?: Subscription;
	constructor(private accountService: AccountService) {

	}

	ngOnInit(): void {

		console.log('this is dashboard',
			this.isAuthenticated())
	}

	ngAfterViewInit() { }

	isAuthenticated(): boolean {
		return this.accountService.isAuthenticated();
	}
}
