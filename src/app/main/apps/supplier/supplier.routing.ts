import { Routes } from "@angular/router";
import { SupplierComponent } from "./supplier.component";
import { OrderComponent } from "./order/order.component";
import { ProductComponent } from "./product/product.component";

export const supplierRoutes: Routes = [
    {
        path: '',
        component: SupplierComponent
    },
    {
        path: 'order',
        component: OrderComponent
    },
    {
        path: 'product',
        component: ProductComponent
    }
]
