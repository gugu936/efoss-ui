import { NgModule } from '@angular/core';

import { MenuItems } from './menu-items/menu-items';
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';
import { EfossSharedLibsModule } from './shared-libs.module';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalComponent } from './login/login.component';
import { SpinnerComponent } from './spinner.component';


@NgModule({
  imports: [EfossSharedLibsModule],
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AlertComponent,
    SpinnerComponent,
    LoginModalComponent,
    AlertErrorComponent,
    AccordionDirective
  ],
  exports: [
    AccordionAnchorDirective,
    EfossSharedLibsModule,
    AlertComponent,
    SpinnerComponent,
    LoginModalComponent,
    AlertErrorComponent,
    AccordionLinkDirective,
    AccordionDirective
  ],
  providers: [MenuItems]
})
export class EfossSharedModule { }
