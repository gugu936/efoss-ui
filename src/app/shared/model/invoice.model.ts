import { Moment } from 'moment';

export interface IInvoice {
  id?: string;
  orderId?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class Invoice implements IInvoice {
  constructor(public id?: string, public orderId?: string, public createdAt?: Moment, public updatedAt?: Moment) {}
}
