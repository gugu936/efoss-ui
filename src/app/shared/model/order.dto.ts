import { Moment } from 'moment';
import { PaymentMethod } from './payment-method.model';
import { Company } from './company.model';
import { BillingAddress } from './billing-address.model';
import { DeliveryAddress } from './delivery-address.model';
import { CartItem } from './cart-item.model';
import { CartItemDTO } from './cart-item.dto';
import { Shipping } from './shipping.model';
import { ShippingMethod } from './shipping-method.model';

export interface IOrderDTO {
  id?: string;
  cartId?: string;
  status?: string;
  cartItems?: CartItemDTO[];
  billingAddress?: BillingAddress;
  deliveryAddress?: DeliveryAddress;
  paymentMethod?: PaymentMethod;
  shippingMethod?: ShippingMethod;
  shipping?: Shipping;
  company?: Company;
  total?: number;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class OrderDTO implements IOrderDTO {
  constructor(
    public id?: string,
    public cartId?: string,
    public status?: string,
    public paymentMethod?: PaymentMethod,
    public shippingMethod?: ShippingMethod,
    public shipping?: Shipping,
    public cartItems?: CartItemDTO[],
    public company?: Company,
    public total?: number,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
