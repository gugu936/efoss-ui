import { Moment } from 'moment';

export interface IAddress {
  id?: string;
  userId?: string;
  telephone?: string;
  address?: string;
  postcode?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class Address implements IAddress {
  constructor(
    public id?: string,
    public userId?: string,
    public telephone?: string,
    public address?: string,
    public postcode?: string,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
