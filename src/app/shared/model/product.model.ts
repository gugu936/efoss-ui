import { Moment } from 'moment';

export interface IProduct {
  id?: string;
  name?: string;
  qty?: number;
  description?: string;
  companyId?: string;
  brand?: string;
  categoryId?: string;
  price?: number;
  refurbished?: boolean;
  size?: string[];
  color?: string[];
  height?: number;
  depth?: number;
  width?: number;
  shippingFee?: number;
  weight?: number;
  active?: boolean;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class Product implements IProduct {
  constructor(
    public id?: string,
    public name?: string,
    public qty?: number,
    public description?: string,
    public companyId?: string,
    public size?: string[],
    public color?: string[],
    public brand?: string,
    public categoryId?: string,
    public price?: number,
    public shippingFee?: number,
    public refurbished?: boolean,
    public height?: number,
    public depth?: number,
    public width?: number,
    public weight?: number,
    public active?: boolean,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {
    this.active = this.active || false;
  }
}
