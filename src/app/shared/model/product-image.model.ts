import { Moment } from 'moment';

export interface IProductImage {
  id?: string;
  productId?: string;
  filename?: string;
  imageContentType?: string;
  image?: any;
  sequence?: number;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class ProductImage implements IProductImage {
  constructor(
    public id?: string,
    public productId?: string,
    public filename?: string,
    public imageContentType?: string,
    public image?: any,
    public sequence?: number,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
