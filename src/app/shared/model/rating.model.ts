export interface IRating {
  id?: string;
  userId?: string;
  productId?: string;
  scores?: number;
}

export class Rating implements IRating {
  constructor(public id?: string, public userId?: string, public productId?: string, public scores?: number) {}
}
