import { Moment } from 'moment';

export interface IOrder {
  id?: string;
  cartId?: string;
  status?: string;
  paymentMethodId?: string;
  companyId?: string;
  cartItemIds?: string[];
  total?: number;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class Order implements IOrder {
  constructor(
    public id?: string,
    public cartId?: string,
    public status?: string,
    public paymentMethodId?: string,
    public companyId?: string,
    public cartItemIds?: string[],
    public total?: number,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
