import { Moment } from 'moment';

export interface IPaymentMethod {
  id?: string;
  name?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class PaymentMethod implements IPaymentMethod {
  constructor(public id?: string, public name?: string, public createdAt?: Moment, public updatedAt?: Moment) {}
}
