import { Moment } from 'moment';

export interface IShippingDTO {
  id?: string;
  orderId?: string;
  trackingCode?: string;
  carrier?: string;
  weight?: number;
  fee?: number;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class ShippingDTO implements IShippingDTO {
  constructor(
    public id?: string,
    public orderId?: string,
    public trackingCode?: string,
    public carrier?: string,
    public weight?: number,
    public fee?: number,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
