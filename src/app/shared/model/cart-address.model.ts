import { Moment } from 'moment';

export interface ICartAddress {
  id?: string;
  cartId?: string;
  firstName?: string;
  lastName?: string;
  phone?: string;
  city?: string;
  street?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class CartAddress implements ICartAddress {
  constructor(
    public id?: string,
    public cartId?: string,
    public firstName?: string,
    public lastName?: string,
    public phone?: string,
    public city?: string,
    public street?: string,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
