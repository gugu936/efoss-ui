import { Moment } from 'moment';
import { ProductDTO } from './product.dto';

export interface ICartItemDTO {
  id?: string;
  cartId?: string;
  product?: ProductDTO;
  qty?: number;
  price?: number;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class CartItemDTO implements ICartItemDTO {
  constructor(
    public id?: string,
    public cartId?: string,
    public product?: ProductDTO,
    public qty?: number,
    public price?: number,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
