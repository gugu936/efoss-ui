import { Moment } from 'moment';

export interface ICustomerOrder {
  id?: string;
  cartId?: string;
  status?: string;
  orderId?: string;
  deliveryAddressId?: string;
  billingAddressId?: string;
  sameAsBilling?: boolean;
  userId?: string;
  paymentMethodId?: string;
  discount?: number;
  total?: number;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class CustomerOrder implements ICustomerOrder {
  constructor(
    public id?: string,
    public cartId?: string,
    public status?: string,
    public orderId?: string,
    public deliveryAddressId?: string,
    public billingAddressId?: string,
    public sameAsBilling?: boolean,
    public userId?: string,
    public paymentMethodId?: string,
    public discount?: number,
    public total?: number,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {
    this.sameAsBilling = this.sameAsBilling || false;
  }
}
