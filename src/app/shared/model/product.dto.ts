import { Moment } from 'moment';
import { Category } from './category.model';
import { Company } from './company.model';

export interface IProductDTO {
  id?: string;
  name?: string;
  qty?: number;
  description?: string;
  size?: string[];
  color?: string[];
  companyId?: Company;
  brand?: string;
  category?: Category;
  price?: number;
  weight?: number;
  refurbished?: boolean;
  height?: number;
  depth?: number;
  width?: number;
  shippingFee?: number;
  active?: boolean;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class ProductDTO implements IProductDTO {
  constructor(
    public id?: string,
    public name?: string,
    public qty?: number,
    public size?: string[],
    public color?: string[],
    public description?: string,
    public companyId?: Company,
    public brand?: string,
    public category?: Category,
    public price?: number,
    public weight?: number,
    public refurbished?: boolean,
    public height?: number,
    public depth?: number,
    public width?: number,
    public shippingFee?: number,
    public active?: boolean,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {
    this.active = this.active || false;
  }
}
