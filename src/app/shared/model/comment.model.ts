import { Moment } from 'moment';

export interface IComment {
  id?: string;
  userId?: string;
  content?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class Comment implements IComment {
  constructor(public id?: string, public userId?: string, public content?: string, public createdAt?: Moment, public updatedAt?: Moment) {}
}
