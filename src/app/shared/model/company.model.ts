import { Moment } from 'moment';

export interface ICompany {
  id?: string;
  name?: string;
  address?: string;
  phone?: string;
  email?: string;
  website?: string;
  createdAt?: Moment;
  updatedAt?: Moment;
}

export class Company implements ICompany {
  constructor(
    public id?: string,
    public name?: string,
    public address?: string,
    public phone?: string,
    public email?: string,
    public website?: string,
    public createdAt?: Moment,
    public updatedAt?: Moment
  ) {}
}
