import { Injectable } from '@angular/core';
import { Authority } from '../constants/authority.constants';

export interface Menu {
  state: string;
  name: string;
  children: any[];
  type: string;
  role: string;
  icon: string;
}

const MENUITEMS = [
  { state: 'shop', name: 'Shop', type: 'link', icon: 'shopping_basket', role: Authority.ANONYMOUS, children: [] },
  {
    state: 'supplier', name: 'Supplier', type: 'group',
    children: [
      {
        icon: 'store',
        name: 'Product',
        state: 'product'
      } , {
        icon: 'attach_money',
        name: 'Order',
        state: 'order'
      }

    ],
    icon: 'person_outline', role: Authority.SUPPLIER
  },
  // { state: 'dashboard', name: 'Dashboard', type: 'link', icon: 'av_timer', role: Authority.ANONYMOUS, children: [] },

  { state: 'button', type: 'link', name: 'Buttons', icon: 'crop_7_5', role: Authority.ANONYMOUS, children: [] },
  { state: 'grid', type: 'link', name: 'Grid List', icon: 'view_comfy', role: Authority.ANONYMOUS, children: [] },
  { state: 'lists', type: 'link', name: 'Lists', icon: 'view_list', role: Authority.ANONYMOUS, children: [] },
  { state: 'menu', type: 'link', name: 'Menu', icon: 'view_headline', role: Authority.ANONYMOUS, children: [] },
  { state: 'tabs', type: 'link', name: 'Tabs', icon: 'tab', role: Authority.ANONYMOUS, children: [] },
  { state: 'stepper', type: 'link', name: 'Stepper', icon: 'web', role: Authority.ANONYMOUS, children: [] },
  {
    state: 'expansion',
    type: 'link',
    name: 'Expansion Panel',
    icon: 'vertical_align_center',
    role: Authority.ANONYMOUS,
    children: []
  },
  { state: 'chips', type: 'link', name: 'Chips', icon: 'vignette', role: Authority.ANONYMOUS, children: [] },
  { state: 'toolbar', type: 'link', name: 'Toolbar', icon: 'voicemail', role: Authority.ANONYMOUS, children: [] },
  {
    state: 'progress-snipper',
    type: 'link',
    name: 'Progress snipper',
    icon: 'border_horizontal'
    , role: Authority.ANONYMOUS, children: []
  },
  {
    state: 'progress',
    type: 'link',
    name: 'Progress Bar',
    role: Authority.ANONYMOUS, children: [],
    icon: 'blur_circular'
  },
  {
    state: 'dialog',
    type: 'link',
    name: 'Dialog',
    icon: 'assignment_turned_in', role: Authority.ANONYMOUS, children: []
  },
  { state: 'tooltip', type: 'link', name: 'Tooltip', icon: 'assistant', role: Authority.ANONYMOUS, children: [] },
  { state: 'snackbar', type: 'link', name: 'Snackbar', icon: 'adb', role: Authority.ANONYMOUS, children: [] },
  { state: 'slider', type: 'link', name: 'Slider', icon: 'developer_mode', role: Authority.ANONYMOUS, children: [] },
  {
    state: 'slide-toggle',
    type: 'link',
    name: 'Slide Toggle',
    icon: 'all_inclusive', role: Authority.ANONYMOUS, children: []
  }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
