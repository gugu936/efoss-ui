export enum Authority {
  ADMIN = 'ROLE_ADMIN',
  SUPPLIER = 'ROLE_SUPPLIER',
  USER = 'ROLE_USER',
  ANONYMOUS = 'ROLE_ANONYMOUS'
}
