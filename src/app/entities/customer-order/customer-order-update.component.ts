import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICustomerOrder, CustomerOrder } from 'app/shared/model/customer-order.model';
import { CustomerOrderService } from './customer-order.service';

@Component({
  selector: 'jhi-customer-order-update',
  templateUrl: './customer-order-update.component.html'
})
export class CustomerOrderUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    cartId: [],
    status: [],
    orderId: [],
    deliveryAddressId: [],
    billingAddressId: [],
    sameAsBilling: [],
    userId: [],
    paymentMethodId: [],
    discount: [],
    total: [],
    createdAt: [],
    updatedAt: []
  });

  constructor(protected customerOrderService: CustomerOrderService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ customerOrder }) => {
      if (!customerOrder.id) {
        const today = moment().startOf('day');
        customerOrder.createdAt = today;
        customerOrder.updatedAt = today;
      }

      this.updateForm(customerOrder);
    });
  }

  updateForm(customerOrder: ICustomerOrder): void {
    this.editForm.patchValue({
      id: customerOrder.id,
      cartId: customerOrder.cartId,
      status: customerOrder.status,
      orderId: customerOrder.orderId,
      deliveryAddressId: customerOrder.deliveryAddressId,
      billingAddressId: customerOrder.billingAddressId,
      sameAsBilling: customerOrder.sameAsBilling,
      userId: customerOrder.userId,
      paymentMethodId: customerOrder.paymentMethodId,
      discount: customerOrder.discount,
      total: customerOrder.total,
      createdAt: customerOrder.createdAt ? customerOrder.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: customerOrder.updatedAt ? customerOrder.updatedAt.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const customerOrder = this.createFromForm();
    if (customerOrder.id !== undefined) {
      this.subscribeToSaveResponse(this.customerOrderService.update(customerOrder));
    } else {
      this.subscribeToSaveResponse(this.customerOrderService.create(customerOrder));
    }
  }

  private createFromForm(): ICustomerOrder {
    return {
      ...new CustomerOrder(),
      id: this.editForm.get(['id'])!.value,
      cartId: this.editForm.get(['cartId'])!.value,
      status: this.editForm.get(['status'])!.value,
      orderId: this.editForm.get(['orderId'])!.value,
      deliveryAddressId: this.editForm.get(['deliveryAddressId'])!.value,
      billingAddressId: this.editForm.get(['billingAddressId'])!.value,
      sameAsBilling: this.editForm.get(['sameAsBilling'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      paymentMethodId: this.editForm.get(['paymentMethodId'])!.value,
      discount: this.editForm.get(['discount'])!.value,
      total: this.editForm.get(['total'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomerOrder>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
