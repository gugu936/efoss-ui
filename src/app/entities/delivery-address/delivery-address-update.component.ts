import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IDeliveryAddress, DeliveryAddress } from 'app/shared/model/delivery-address.model';
import { DeliveryAddressService } from './delivery-address.service';

@Component({
  selector: 'jhi-delivery-address-update',
  templateUrl: './delivery-address-update.component.html'
})
export class DeliveryAddressUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    userId: [],
    firstName: [],
    lastName: [],
    phone: [],
    city: [],
    street: [],
    createdAt: [],
    updatedAt: []
  });

  constructor(
    protected deliveryAddressService: DeliveryAddressService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ deliveryAddress }) => {
      if (!deliveryAddress.id) {
        const today = moment().startOf('day');
        deliveryAddress.createdAt = today;
        deliveryAddress.updatedAt = today;
      }

      this.updateForm(deliveryAddress);
    });
  }

  updateForm(deliveryAddress: IDeliveryAddress): void {
    this.editForm.patchValue({
      id: deliveryAddress.id,
      userId: deliveryAddress.userId,
      firstName: deliveryAddress.firstName,
      lastName: deliveryAddress.lastName,
      phone: deliveryAddress.phone,
      city: deliveryAddress.city,
      street: deliveryAddress.street,
      createdAt: deliveryAddress.createdAt ? deliveryAddress.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: deliveryAddress.updatedAt ? deliveryAddress.updatedAt.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const deliveryAddress = this.createFromForm();
    if (deliveryAddress.id !== undefined) {
      this.subscribeToSaveResponse(this.deliveryAddressService.update(deliveryAddress));
    } else {
      this.subscribeToSaveResponse(this.deliveryAddressService.create(deliveryAddress));
    }
  }

  private createFromForm(): IDeliveryAddress {
    return {
      ...new DeliveryAddress(),
      id: this.editForm.get(['id'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      phone: this.editForm.get(['phone'])!.value,
      city: this.editForm.get(['city'])!.value,
      street: this.editForm.get(['street'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDeliveryAddress>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
