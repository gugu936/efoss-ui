import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDeliveryAddress } from 'app/shared/model/delivery-address.model';

type EntityResponseType = HttpResponse<IDeliveryAddress>;
type EntityArrayResponseType = HttpResponse<IDeliveryAddress[]>;

@Injectable({ providedIn: 'root' })
export class DeliveryAddressService {
  public resourceUrl = SERVER_API_URL + 'api/delivery-addresses';

  constructor(protected http: HttpClient) {}

  create(deliveryAddress: IDeliveryAddress): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(deliveryAddress);
    return this.http
      .post<IDeliveryAddress>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(deliveryAddress: IDeliveryAddress): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(deliveryAddress);
    return this.http
      .put<IDeliveryAddress>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IDeliveryAddress>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IDeliveryAddress[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(deliveryAddress: IDeliveryAddress): IDeliveryAddress {
    const copy: IDeliveryAddress = Object.assign({}, deliveryAddress, {
      createdAt: deliveryAddress.createdAt && deliveryAddress.createdAt.isValid() ? deliveryAddress.createdAt.toJSON() : undefined,
      updatedAt: deliveryAddress.updatedAt && deliveryAddress.updatedAt.isValid() ? deliveryAddress.updatedAt.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((deliveryAddress: IDeliveryAddress) => {
        deliveryAddress.createdAt = deliveryAddress.createdAt ? moment(deliveryAddress.createdAt) : undefined;
        deliveryAddress.updatedAt = deliveryAddress.updatedAt ? moment(deliveryAddress.updatedAt) : undefined;
      });
    }
    return res;
  }
}
