import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IShipping } from 'app/shared/model/shipping.model';

type EntityResponseType = HttpResponse<IShipping>;
type EntityArrayResponseType = HttpResponse<IShipping[]>;

@Injectable({ providedIn: 'root' })
export class ShippingService {
  public resourceUrl = SERVER_API_URL + 'api/shippings';

  constructor(protected http: HttpClient) {}

  create(shipping: IShipping): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shipping);
    return this.http
      .post<IShipping>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(shipping: IShipping): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(shipping);
    return this.http
      .put<IShipping>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: string): Observable<EntityResponseType> {
    return this.http
      .get<IShipping>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IShipping[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: string): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(shipping: IShipping): IShipping {
    const copy: IShipping = Object.assign({}, shipping, {
      createdAt: shipping.createdAt && shipping.createdAt.isValid() ? shipping.createdAt.toJSON() : undefined,
      updatedAt: shipping.updatedAt && shipping.updatedAt.isValid() ? shipping.updatedAt.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? moment(res.body.createdAt) : undefined;
      res.body.updatedAt = res.body.updatedAt ? moment(res.body.updatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((shipping: IShipping) => {
        shipping.createdAt = shipping.createdAt ? moment(shipping.createdAt) : undefined;
        shipping.updatedAt = shipping.updatedAt ? moment(shipping.updatedAt) : undefined;
      });
    }
    return res;
  }
}
