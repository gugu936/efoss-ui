import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBillingAddress, BillingAddress } from 'app/shared/model/billing-address.model';
import { BillingAddressService } from './billing-address.service';

@Component({
  selector: 'jhi-billing-address-update',
  templateUrl: './billing-address-update.component.html'
})
export class BillingAddressUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    userId: [],
    firstName: [],
    lastName: [],
    phone: [],
    city: [],
    street: [],
    createdAt: [],
    updatedAt: []
  });

  constructor(protected billingAddressService: BillingAddressService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ billingAddress }) => {
      if (!billingAddress.id) {
        const today = moment().startOf('day');
        billingAddress.createdAt = today;
        billingAddress.updatedAt = today;
      }

      this.updateForm(billingAddress);
    });
  }

  updateForm(billingAddress: IBillingAddress): void {
    this.editForm.patchValue({
      id: billingAddress.id,
      userId: billingAddress.userId,
      firstName: billingAddress.firstName,
      lastName: billingAddress.lastName,
      phone: billingAddress.phone,
      city: billingAddress.city,
      street: billingAddress.street,
      createdAt: billingAddress.createdAt ? billingAddress.createdAt.format(DATE_TIME_FORMAT) : null,
      updatedAt: billingAddress.updatedAt ? billingAddress.updatedAt.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const billingAddress = this.createFromForm();
    if (billingAddress.id !== undefined) {
      this.subscribeToSaveResponse(this.billingAddressService.update(billingAddress));
    } else {
      this.subscribeToSaveResponse(this.billingAddressService.create(billingAddress));
    }
  }

  private createFromForm(): IBillingAddress {
    return {
      ...new BillingAddress(),
      id: this.editForm.get(['id'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      phone: this.editForm.get(['phone'])!.value,
      city: this.editForm.get(['city'])!.value,
      street: this.editForm.get(['street'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? moment(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      updatedAt: this.editForm.get(['updatedAt'])!.value ? moment(this.editForm.get(['updatedAt'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBillingAddress>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
