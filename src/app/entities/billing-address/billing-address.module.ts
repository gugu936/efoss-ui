import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EfossSharedModule } from 'app/shared/shared.module';
import { BillingAddressComponent } from './billing-address.component';
import { BillingAddressDetailComponent } from './billing-address-detail.component';
import { BillingAddressUpdateComponent } from './billing-address-update.component';
import { BillingAddressDeleteDialogComponent } from './billing-address-delete-dialog.component';
import { billingAddressRoute } from './billing-address.route';

@NgModule({
  imports: [EfossSharedModule, RouterModule.forChild(billingAddressRoute)],
  declarations: [
    BillingAddressComponent,
    BillingAddressDetailComponent,
    BillingAddressUpdateComponent,
    BillingAddressDeleteDialogComponent
  ],
  entryComponents: [BillingAddressDeleteDialogComponent]
})
export class EfossBillingAddressModule {}
