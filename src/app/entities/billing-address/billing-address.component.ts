import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBillingAddress } from 'app/shared/model/billing-address.model';
import { BillingAddressService } from './billing-address.service';
import { BillingAddressDeleteDialogComponent } from './billing-address-delete-dialog.component';

@Component({
  selector: 'jhi-billing-address',
  templateUrl: './billing-address.component.html'
})
export class BillingAddressComponent implements OnInit, OnDestroy {
  billingAddresses?: IBillingAddress[];
  eventSubscriber?: Subscription;

  constructor(
    protected billingAddressService: BillingAddressService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.billingAddressService.query().subscribe((res: HttpResponse<IBillingAddress[]>) => (this.billingAddresses = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBillingAddresses();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBillingAddress): string {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBillingAddresses(): void {
    this.eventSubscriber = this.eventManager.subscribe('billingAddressListModification', () => this.loadAll());
  }

  delete(billingAddress: IBillingAddress): void {
    const modalRef = this.modalService.open(BillingAddressDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.billingAddress = billingAddress;
  }
}
